* [ ] The app complies with the [inclusion citeria](https://f-droid.org/wiki/page/Inclusion_Policy).
* [ ] The app is not already [listed](https://gitlab.com/search?scope=issues&group_id=28397) in the repo or issue tracker.
* [ ] The app has not already [been requested](https://gitlab.com/search?scope=issues&project_id=2167965)
* [ ] The original app author has been notified, and supports the inclusion.

---------------------

F-Droid metadata template: See
[Build Metadata Reference](https://f-droid.org/docs/Build_Metadata_Reference)
for more details. Single-line fields start right after the colon
(without whitespace).

---------------------

#### PACKAGE ID:
<!-- Each app has a unqiue id, you will find it in files called AndroidManifest.xml or build.gradle most of the time. -->

#### Name:
<!-- The Name of the app. -->

#### Categories:
<!-- Comma-separated values, no space between categories, see https://gitlab.com/fdroid/fdroiddata/blob/master/stats/categories.txt for valid values. -->

#### License:
<!-- Comma-separated values, no space between licenses, see https://gitlab.com/fdroid/fdroiddata/blob/master/stats/licenses.txt for existing values, use https://spdx.org/licenses/ short identifiers, must be floss-compatible. -->

#### Web Site:
<!-- A link to the website, sometimes linking to the README file might be appropriated, use HEAD "branch". -->

#### Source Code:
<!-- A link to the web-browsable code repository. -->

#### Issue Tracker:
<!-- A link to the issue tracker. -->

#### Donate:
<!-- A web link proividing a landing pagage for donations. -->

#### FlattrID:
<!-- The FlattrId number. -->

#### Bitcoin:
<!-- The Bitcoin address of the author, please verify it is listed in the used repo. -->

#### Summary:
<!-- One sentence, no more than 30-50 chars, no trailing punctuation, focus on actions what the users does with the app, e.g. "Read and send emails" instead of "Email client". -->

#### Description:
<!-- 
Description of what the app does, starting on a new line. It should be as
objective as possible and wrapped at 80 chars (except links and list items).

A blank line means a line break, i.e. the end of a paragraph.

Bulleted lists can be used:

* Item 1
* Item 2

Links can be added like this:
[https://github.com/org/project/raw/HEAD/res/raw/changelog.xml Changelog]

Links to other apps too: [[some.other.app]]

To close a multiline field, add a new line with only a dot.
--> 
#### .

#### Repo Type:
<!-- git, git-svn, svn, hg or bzr -->

#### Repo:
<!-- repo url, preferably https -->

#### Build:
<!-- <version>,<version code> -->
<!--
For a detailed explanation see the manual http://f-droid.org/manual. Most common values for gradle builds are:
     commit=<tag/commit/rev>
     subdir=app
     gradle=yes
--> 

#### Maintainer Notes:
<!--
Here go the notes to take into account for future updates, builds, etc.
Will be published in the wiki if present.
-->
#### .

#### Auto Update Mode:
<!-- If you are not really sure don't change it. -->
None

#### Update Check Mode:
<!-- Set it to "Tags" if you know the repo uses tags. -->
RepoManifest

#### Current Version:
<!-- The most current version name (string). -->
1.0

#### Current Version Code:
<!-- The most current version code (integer). -->
1
